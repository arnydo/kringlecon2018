# Quick Answers

## Question 1
What phrase is revealed when you answer all of the KringleCon Holiday Hack History questions?

!!! success "Answer"
    Happy Trails

## Question 2
Who submitted (First Last) the rejected talk titled Data Loss for Rainbow Teams: A Path in the Darkness?

!!! success "Answer"
    John McClane

## Question 3
The KringleCon Speaker Unpreparedness room is a place for frantic speakers to furiously complete their presentations. The room is protected by a door passcode. Upon entering the correct passcode, what message is presented to the speaker?

!!! success "Answer"
    Welcome unprepared speaker!

## Question 4
Retrieve the encrypted ZIP file from the North Pole Git repository. What is the password to open this file?

!!! success "Answer"
    Yippee-ki-yay

## Question 5
Using the data set contained in this SANS Slingshot Linux image, find a reliable path from a Kerberoastable user to the Domain Admins group. What’s the user’s logon name (in username@domain.tld format)?

!!! success "Answer"
    LDUBEJ00320@AD.KRINGLECASTLE.COM

## Question 6
Bypass the authentication mechanism associated with the room near Pepper Minstix. A sample employee badge is available. What is the access control number revealed by the door authentication panel?

!!! success "Answer"
    19880715

## Question 7
Santa uses an Elf Resources website to look for talented information security professionals. Gain access to the website and fetch the document C:\candidate_evaluation.docx. Which terrorist organization is secretly supported by the job applicant whose name begins with "K"?

!!! success "Answer"
    Fancy Beaver
 
## Question 8
Santa has introduced a web-based packet capture and analysis tool to support the elves and their information security work. Using the system, access and decrypt HTTP/2 network activity. What is the name of the song described in the document sent from Holly Evergreen to Alabaster Snowball?

!!! success "Answer"
    mary had a little lamb

## Question 9
Alabaster Snowball is in dire need of your help. Santa's file server has been hit with malware. Help Alabaster Snowball deal with the malware on Santa's server by completing several tasks. ...create a rule that will catch all new infections. What is the success message displayed by the Snort terminal?

!!! success "Answer"
    alert udp any any -> any any ( msg:"Malware"; content:"77616E6E61636F6F6B69652E6D696E2E707331"; sid:00001; rev:1; ) 
 
## Question 10
After completing the prior question, Alabaster gives you a document he suspects downloads the malware. What is the domain name the malware in the document downloads from?

!!! success "Answer"
    erohetfanu.com
 
## Question 11
Analyze the full malware source code to find a kill-switch and activate it at the North Pole's domain registrar HoHoHo Daddy. What is the full sentence text that appears on the domain registration success message (bottom sentence)?

!!! success "Answer"
    Successfully registered yippeekiyaa.aaay!
 
## Question 12
After activating the kill-switch domain in the last question, Alabaster gives you a zip file with a memory dump and encrypted password database. Use these files to decrypt Alabaster's password database. What is the password entered in the database for the Vault entry?

!!! success "Answer"
    ED#ED#EED#EF#G#F#G#ABA#BA#B

## Question 13
Use what you have learned from previous challenges to open the door to Santa's vault. What message do you get when you unlock the door?

!!! success "Answer"
    You have unlocked Santa's vault!
 
## Question 14
Who was the mastermind behind the whole KringleCon plan?

!!! success "Answer"
    santa

