# Hints

## VI Editor Basics
* From Bushy Evergreen
* [https://kb.iu.edu/d/afcz](https://kb.iu.edu/d/afcz)

## Powershell Command Injection
* From Minty Candycane
* [https://ss64.com/ps/call.html](https://ss64.com/ps/call.html)

## Website Directory Browsing
* From: Minty Candycane
* [https://portswigger.net/kb/issues/00600100_directory-listing](https://portswigger.net/kb/issues/00600100_directory-listing)

## Finding Browsable Directories
* From: Minty Candycane
* On a website, finding browsable directories is sometimes as simple as removing characters from the end of a URL.

## SQLite3 .dump'ing
* From: Minty Candycane
* [SQLite3 Data Dump](https://www.digitalocean.com/community/questions/how-do-i-dump-an-sqlite-database)

## Vim Artifacts
* From: Tangle Coalbox
* [Forensic Relevance of Vim Artifacts](https://tm4n6.com/2017/11/15/forensic-relevance-of-vim-artifacts/)

## de Bruijn Sequence Generator
* From: Tangle Coalbox
* [de Bruijn sequence generator](http://www.hakank.org/comb/debruijn.cgi)

## Opening a Ford Lock Code
* From: Tangle Coalbox
* [Opening a Ford with a Robot and the de Bruijn Sequence](https://hackaday.com/2018/06/18/opening-a-ford-with-a-robot-and-the-de-bruijn-sequence/)

## Trufflehog Tool
* From: Wunorse Openslae
* [Trufflehog](https://github.com/dxa4481/truffleHog)

## Bloodhound Tool
* From: Holly Evergreen
* [Bloodhound Tool](https://github.com/BloodHoundAD/BloodHound)