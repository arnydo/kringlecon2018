# Easter Eggs

## Ventiltion Diagram
Many people found themselves trying to navigate their way through the ventialtion shafts (including myself) without much luck. Those that were able to pull out the graph paper and carefully track each step were able to make it through.

However, if looking closely through the GIT repo in Objective 4: Data Repo Analysis [the git repo](https://git.kringlecastle.com/Upatree/santas_castle_automation) contains a ZIP file called [ventilation_diagram.zip](https://git.kringlecastle.com/Upatree/santas_castle_automation/blob/master/schematics/ventilation_diagram.zip).
Downloading this file and using the password found hidden in that repo's commit history provides two diagrams, one for each level of the ventilation puzzle!

![Ventilation Floor 1](https://gitlab.com/arnydo/kringlecon2018/raw/master/docs/media/ventilation_diagram_1F.jpg)
![Ventilation Floor 2](https://gitlab.com/arnydo/kringlecon2018/raw/master/docs/media/ventilation_diagram_2F.jpg)

## MissingNo.

Throughout the time clicking through network stats and seeing how different parts of the game are layed out, the "egg.png" file stood out. Viewing the image revealed an odd "backwards L" shape that really didn't seem to mean anything. I never saw this appear throughout the challenges but surely it was there for a reason.

A quick Internet search revealed that this image is actually the "MissingNo." Pokemon the debued in the Pokemon Red and Blue games starting back in 1999. This was actually a glitch that occured when a series of events occured in a particular order within the game. Accessing this glitch could potentially cause the game to corrupt! The glitch drew much attention from its fanbase to the point that Nintendo began including it in other Pokemon games and people even offered money for people to help locate this glitch. Some say this was "one of the most popular glitches ever in game history".

I only had a chance to play Pokemon Yellow so this is the first time I came across this reference...

Digging throught he JS source you can see a reference to this which would render a div element with the class "a-wild-missingno-appears" if a particular DNA (avatar) sequence was chosen.

```javascript hl_lines="7 9"
render() {
                const {
                    dna: e,
                    version: t
                } = this.props;
                if (au.a.validateSequence(e)) return du.a.createElement("div", {
                    className: "a-wild-missingno-appears"
                }, du.a.createElement("img", {
                    src: "images/egg.png",
                    alt: ":)"
                }));
```

Looking at what the "validateSquence" function does it is expecting either 60 pairs of AT, TA, GC, or CG.

```javascript hl_lines="6"
validateSequence(e) {
    if (!e) return P;
    if ('' == `${e
    }
    `) return P;
    if (!/((?:AT|TA|GC|CG){60})/i.test(e)) return P;
    const t = j.getSize();
    return e.length / 8 !== t ? P : !!function (e, t) {
    return (Object(l.a) (e) ? i.a : u) (e, Object(s.a) (t, 3))
    }(j.decodeDNASequence(e), (e, t) =>{
    if ('size' === t) return 0 > e || e >= x.length;
    if (_(Object.keys(O), t)) return 0 > e || e >= O[t].count;
    const n = Object(w.a) (C, {
    name: t
});
```

So we need to find out what avatar configuration would create this sequence. We can use this great graphic to assist.

I modified the WebSocket query in attempt to trigger MissingNo. but it didn't seem to work...

```
{"type":"WS_USERS","users":{"xxxx":{"gdprDocId":"xxxx","email":"xxxxxxx","username":"xxxxx","avatar":"ATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATATAT","country":"US"}},"initialLogin":true}
```

![MissingNo](https://gitlab.com/arnydo/kringlecon2018/raw/master/docs/media/Missingno.png)

[MissingNo](https://en.wikipedia.org/wiki/MissingNo.)