# Introduction

## A little about me

Hi! I am Kyle Parrish, a Cyber Security Engineer in The Villages, Florida, Florida's Friendliest Hometown!

I have been in this role now for the past year after two years as a Server Operations Engineer. During those two years I did not have much of a desire to pursue Cyber Security as a career but that quickly changed when the Directory of Server Operations voiced his desire to train me up as a Cyber Security Engineer. With this, I quickly learned of the many oportunities involved in this path and seemed to constantly be looking for new tools, techniques and oportunities to learn more and grow in my understanding of Cyber Security.


## SANS & KringleCon

SANS has played a huge role in my desire to learn more. I attended my first SANS training this year and at the GIAC GSEC certification. Bryce Galbryth was an excellent instructor and introduced many valuable resources that I now use daily.

At SANS is where I attended my first CTF style challenge. I decided to step out of my comfort zone and signed up for Cyber Defense NetWars with no anticipation that I would place in the event but figured it would be a good experience. I managaged to maintain first place on the board for the duration of the event until the final 15 minutes where I dropped down to third place. Netwars indeed was a fantastic experience and walked away with new scenarios and techniques that I can use in the workplace.

Following Netwars I have been looking for additional CTF challenges and that is when I came across KringleCon. KringleCon 2018 was my first HolidayHackChallenge and far exceeded my expectations. The event was very well organized, creating a unique security convention feel full of practical presentations by well-known security professionals and real-world challenges.

Working through these challenges have been difficult but the effor has certainly paid off with the satisfaction of completing the event and having expanded my toolset and understanding of these scenarios just a little but more.

This review and walkthrough is my take on the challenges and how I decided to takle them as well as the difficulties and discoveries that were faced.

You can view some of my tools and scripts that were used along the way at [Gitlab Repo](https://gitlab.com/arnydo/kringlecon2018)